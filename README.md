**Français**

Le projet OralChess est un projet étudiant open source dont le principe est d'utiliser la voix pour jouer une partie d'échec.  
La reconnaissance vocale du jeu est basé sur le projet open source de Mozilla, Deepspeech.  
Il utilise également Python-chess pour la vérification des coups et Stockfish, pour jouer des parties Player Vs IA. 

**Auteurs**  

Equipe OralChess  


**Prerequisites**  
  
* Python 3.5  
* Deepspeech: https://github.com/mozilla/DeepSpeech
* Python-chess : https://python-chess.readthedocs.io/en/latest/  
* Stockfish : https://github.com/official-stockfish/Stockfish

**Installation**  
  
Afin de l'installer vous devez suivre les étapes du fichier Install.txt.

**Utilisation du jeu**  

L'ensemble des ordres donnés avec la voix doivent être en anglais.  

*Menu*  
Au lancement du logiciel, le menu du jeu s'affiche. Afin de naviguer dans ce menu, vous devez prononcer un nombre entre 1 et 7, an anglais:  

* 1 : Lancer une partie Joueur VS IA
* 2 : Lancer une partie Joueur VS Joueur
* 3 : Charger une partie Joueur VS IA
* 4 : Charger une partie Joueur VS Joueur
* 5 : Replay de la dernière partie contre l'IA
* 6 : Afficher les règles du jeu  
* 7 : Quitter le jeu et eteindre la PI  

De plus, les règles du jeu s'affichent pendant 20 secondes.  
Le Replay rejoue l'ensemble des coups de la dernière partie jouée contre l'IA. Il y a une pause de 5 secondes entre chaque coup joué

*En partie*  
Lorsqu'une partie est en cours, vous devez donner votre déplacement quand le bouton d'enregistrement est **vert**.  
De plus, vous avez *4 secondes* pour donner votre ordre de mouvement. Cet ordre de mouvement est composé de :  
* Chiffre entre 1-8 : pour la colonne de la case de départ de la pièce à déplacer
* Chiffre entre 1-8 : pour la ligne de la case de départ de la pièce à déplacer
* Chiffre entre 1-8 : pour la colonne de la case d'arrivée de la pièce déplacé
* Chiffre entre 1-8 : pour la ligne de la case d'arrivée de la pièce déplacé  

Si le mouvement est bien interprété par le logiciel, le mouvement s'effectuera et la main sera alors soit à l'IA soit au Joueur 2.  
Si au contraire, celui-ci n'est pas bien interprété, le joueur devra alors redonner son ordre de mouvement au logiciel.

**English** 

The OralChess project is an open source student project whose principle is to use the voice to play a chess game.
The game's voice recognition is based on Mozilla's open source project, Deepspeech.
It also uses Python-chess for checking moves and Stockfish, to play AI Player Vs games.

**Author**  

OralChess Team


**Prerequisites**  
  
* Python 3.5  
* Deepspeech: https://github.com/mozilla/DeepSpeech
* Python-chess : https://python-chess.readthedocs.io/en/latest/  
* Stockfish : https://github.com/official-stockfish/Stockfish

**Installation**  
  

In order to install it you must follow the steps of the file Install.txt.

**Game manual**  

All orders given with the voice must be in English.

*Menu*  
When the software is launched, the game menu is displayed. In order to navigate in this menu, you must pronounce a number between 1 and 7, in English:
* 1 : Start a game Player VS IA
* 2 : Start a game Player VS Player
* 3 : Load a game Player VS IA
* 4 : Load a game Player VS Player
* 5 : Replay of the last game Player VS IA
* 6 : Display rules  
* 7 : Quit game and switch off PI

In addition, the rules of the game are displayed for 20 seconds.
The Replay plays all the moves of the last game played against the AI. There is a pause of 5 seconds between each move played

*In game*  
When a game is in progress, you must say your move when the record button is ** green **.
Furthermore, you have * 4 seconds * to give your movement order. This movement order is composed of:  

* Number between 1-8 : for the column from the starting square of the pawn to be moved
* Number between 1-8 : for the line from the starting square of the pawn to be moved
* Number between 1-8 : for the column from the arrival square of the pawn to be moved
* Number between 1-8 : for the line from the arrival square of the pawn to be moved

If the movement is well interpreted by the software, the movement will be made and the turn will be either to the AI or to Player 2.
If on the contrary, this one is not well interpreted, the player will have then to give back his order of movement to the software.






import chess

f = open("fen.txt","r")
last_line = f.readlines()[-1]
f.close()

board = chess.Board(fen=last_line, chess960=True)
print(board)

f = open("move.txt","r") 
tmp = f.readlines()[-1]
f.close()

move = tmp.split("\n")

if (move[0] == "WrongCommand") or (move[0] == ""):
	f = open("moveOK.txt","w")
	print("NOK")
	f.write("NOK")
	f.close()
else:
	b = chess.Move.from_uci(move[0]) in board.legal_moves
	print(board.legal_moves)

	f = open("moveOK.txt","w")
	if b == True:
		print("OK")
		f.write("OK")
	else:
		print("NOK")
		f.write("NOK")
	f.close()
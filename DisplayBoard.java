import java.awt.*;
import java.awt.image.BufferedImage;
import javax.swing.*;
import javax.swing.border.*;
import javax.imageio.ImageIO;
import java.io.File;
import java.awt.Color;

public class DisplayBoard {

    private final JPanel gui = new JPanel(new BorderLayout(3, 3));
    private JButton[][] chessBoardSquares = new JButton[8][8];
    private Image[][] chessPieceImages = new Image[2][6];
    private JPanel chessBoard;
    private static final String COLS = "12345678";
    public static final int QUEEN = 0, KING = 1,
            ROOK = 2, KNIGHT = 3, BISHOP = 4, PAWN = 5;
    public static final int[] STARTING_ROW = {
        ROOK, KNIGHT, BISHOP, KING, QUEEN, BISHOP, KNIGHT, ROOK
    };
    public static final int BLACK = 0, WHITE = 1;
    JFrame f = new JFrame("OralChess");
    JButton status,record,iaplay,movesaid,texte;


    public DisplayBoard() {
        f.add(this.getGui());
        f.setTitle("OralChess Board");
        f.setResizable(false);
        f.setBackground(new Color(100, 100, 100));
        f.setSize(1280, 800);
        f.setLocation(0, 0);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        initializeDisplayBoard();
    }

    public final void initializeDisplayBoard() {
        // create the images for the chess pieces
        createImages();
        BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);

        // Create a new blank cursor.
        Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
        cursorImg, new Point(0, 0), "blank cursor");
        // Set the blank cursor to the JFrame.
        f.getContentPane().setCursor(blankCursor);
        chessBoard = new JPanel(new GridLayout(0, 9)) {
            @Override
            public final Dimension getPreferredSize() {
                /*Dimension d = super.getPreferredSize();
                Dimension prefSize = null;
                Component c = getParent();
                if (c == null) {
                    prefSize = new Dimension(
                            (int)d.getWidth(),(int)d.getHeight());
                } else if (c!=null &&
                        c.getWidth()>d.getWidth() &&
                        c.getHeight()>d.getHeight()) {
                    prefSize = c.getSize();
                } else {
                    prefSize = d;
                }
                int w = (int) prefSize.getWidth();
                int h = (int) prefSize.getHeight();
                // the smaller of the two sizes
                int s = (w>h ? h : w);*/
                return new Dimension(700,700);
            }
        };
        /*chessBoard.setBorder(new CompoundBorder(
                new EmptyBorder(8,8,8,8),
                new LineBorder(Color.BLACK)
                ));*/
        // set up the main GUI
        texte = new JButton("Ready to play!");
        texte.setBounds(1020,200,200,25);
        texte.setVisible(false);
        gui.add(texte);
        status = new JButton("Normal");
        status.setBounds(50,400,150,25);
        status.setVisible(false);
        gui.add(status);
        record = new JButton();
        record.setBounds(1100,400,50,50);
        record.setBackground(Color.RED);
        record.setVisible(false);
        gui.add(record);
        iaplay = new JButton("Loading");
        iaplay.setBounds(1050,455,150,25);
        /*try {
                System.out.println("coucou");
                BufferedImage bi = ImageIO.read(new File("loading2.gif"));
                Icon icon = new ImageIcon(bi);
                iaplay.setIcon(icon);
            } catch (Exception e) {
                e.printStackTrace();
            }*/
        iaplay.setVisible(false);
        gui.add(iaplay);
        movesaid = new JButton();
        movesaid.setBounds(30,200,200,25);
        movesaid.setVisible(false);
        gui.add(movesaid);
        Color gr = new Color(0,100,0);
        chessBoard.setBackground(gr);
        JPanel boardConstrain = new JPanel(new GridBagLayout());
        boardConstrain.setBackground(gr);
        boardConstrain.add(chessBoard);
        gui.add(boardConstrain);

        // create the chess board squares
        Insets buttonMargin = new Insets(0, 0, 0, 0);
        for (int i = 0; i < chessBoardSquares.length; i++) {
            for (int j = 0; j < chessBoardSquares[i].length; j++) {
                JButton b = new JButton();
                b.setMargin(buttonMargin);
                // our chess pieces are 64x64 px in size, so we'll
                // 'fill this in' using a transparent icon..
                ImageIcon icon = new ImageIcon(
                        new BufferedImage(64, 64, BufferedImage.TYPE_INT_ARGB));
                b.setIcon(icon);
                if ((j % 2 == 1 && i % 2 == 1)
                        //) {
                        || (j % 2 == 0 && i % 2 == 0)) {
                    b.setBackground(Color.WHITE);
                } else {
                    b.setBackground(Color.BLACK);
                }
                chessBoardSquares[j][i] = b;
            }
        }

        /*
         * fill the chess board
         */
        chessBoard.add(new JLabel(""));
        // fill the top row
        for (int i = 0; i < 8; i++) {
            chessBoard.add(
                    new JLabel(COLS.substring(i, i + 1),
                    SwingConstants.CENTER));
        }
        // fill the black non-pawn piece row
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                switch (j) {
                    case 0:
                        chessBoard.add(new JLabel("" + (9-(i + 1)),
                                SwingConstants.CENTER));
                    default:
                        chessBoard.add(chessBoardSquares[j][i]);
                }
            }
        }
        texte.setVisible(true);
        movesaid.setVisible(true);
        iaplay.setVisible(false);
        record.setVisible(true);
        status.setVisible(true);
    }

    public final JComponent getGui() {
        return gui;
    }

    private final void createImages() {
        try {
            BufferedImage bi = ImageIO.read(new File("Images/pieces.png"));
            for (int i = 0; i < 2; i++) {
                for (int j = 0; j < 6; j++) {
                    chessPieceImages[i][j] = bi.getSubimage(
                            j * 64, i * 64, 64, 64);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Initializes the icons of the initial chess board piece places
     */
    public final void PrintChessBoard(char chessBoard[][],boolean whichPlayer, boolean pvpGame) {
        if(pvpGame) {
            if(!whichPlayer) {
               texte.setText("Player 1: say a move");
            } else {
                texte.setText("Player 2: say a move");
            }   
        } else {
            if(whichPlayer) {
                texte.setText("Player 1: say a move");
            } else {
                texte.setText("IA is thiking");
            }
        }
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++ ) {
                if(chessBoard[i][j] == 'R') {
                     chessBoardSquares[j][i].setIcon(new ImageIcon(chessPieceImages[WHITE][ROOK]));
                } else if(chessBoard[i][j] == 'N') {
                    chessBoardSquares[j][i].setIcon(new ImageIcon(chessPieceImages[WHITE][KNIGHT]));
                } else if(chessBoard[i][j] == 'B') {
                    chessBoardSquares[j][i].setIcon(new ImageIcon(chessPieceImages[WHITE][BISHOP]));
                } else if(chessBoard[i][j] == 'Q') {
                    chessBoardSquares[j][i].setIcon(new ImageIcon(chessPieceImages[WHITE][KING]));
                } else if(chessBoard[i][j] == 'K') {
                    chessBoardSquares[j][i].setIcon(new ImageIcon(chessPieceImages[WHITE][QUEEN]));
                } else if(chessBoard[i][j] == 'P') {
                    chessBoardSquares[j][i].setIcon(new ImageIcon(chessPieceImages[WHITE][PAWN]));
                } else if(chessBoard[i][j] == 'r') {
                    chessBoardSquares[j][i].setIcon(new ImageIcon(chessPieceImages[BLACK][ROOK]));
                } else if(chessBoard[i][j] == 'n') {
                   chessBoardSquares[j][i].setIcon(new ImageIcon(chessPieceImages[BLACK][KNIGHT]));
                } else if(chessBoard[i][j] == 'b') {
                    chessBoardSquares[j][i].setIcon(new ImageIcon(chessPieceImages[BLACK][BISHOP]));
                } else if(chessBoard[i][j] == 'q') {
                    chessBoardSquares[j][i].setIcon(new ImageIcon(chessPieceImages[BLACK][KING]));
                } else if(chessBoard[i][j] == 'k') {
                    chessBoardSquares[j][i].setIcon(new ImageIcon(chessPieceImages[BLACK][QUEEN]));
                } else if(chessBoard[i][j] == 'p') {
                    chessBoardSquares[j][i].setIcon(new ImageIcon(chessPieceImages[BLACK][PAWN]));
                }
                else chessBoardSquares[j][i].setIcon(null);
            }
            
        }
    }

    /**
    * Display the status of the game : Check, Checkmate, Stalemate, Gameover
    */
    public void displayStatus(String stat) {
        if(stat.equals("check")) {
            status.setBackground(Color.YELLOW);
            status.setText("Check !");
        } else if(stat.equals("checkmate")) {
            status.setBackground(Color.RED);
            status.setText("Checkmate");
        } else if(stat.equals("stalemate")) {
            status.setBackground(Color.ORANGE);
            status.setText("Stalemate");
        } else if(stat.equals("gameover")) {
            status.setBackground(Color.ORANGE);
            status.setText("Game Over");
        }
        else
        {
            status.setBackground(Color.GREEN);
            status.setText("Normal");
        }
    }

    /**
    * Indicate if the user can speak or not
    */
    public void displayRecording(boolean isRecording) {
        if(isRecording) {
                record.setBackground(Color.GREEN);
            } else {
                record.setBackground(Color.RED);
            }
    }

    public void displayLoading(boolean isLoading)
    {
        if(isLoading) {
            iaplay.setVisible(true);
        } else {
            iaplay.setVisible(false);
        }   
    }

    public void displayMove(String tellmove)
    {        
        movesaid.setText(tellmove);
    }

    public void displayNone()
    {
        texte.setVisible(false);
        movesaid.setVisible(false);
        iaplay.setVisible(false);
        record.setVisible(false);
        status.setVisible(false);
    }

    public void close()
    {
        f.dispose();
    }
}


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileWriter;

import java.lang.Runtime;

import java.util.Scanner;

public class GameLoop {

	DisplayBoard d;

	private char chessBoard[][] = new char[8][8];
	String EnPassant; //contains the move authorized for en passant
	boolean whiteRoqueQS = true; //Roque available for white queen side
	boolean whiteRoqueKS = true; //Roque available for white king side
	boolean blackRoqueQS = true; //Roque available for black queen side
	boolean blackRoqueKS = true; //Roque available for black king side
	String status="NORMAL"; //NORMAL, CHECK, CHECKMATE, STALEMATE, GAMEOVER
	boolean white = false; // by default white is 0
	boolean black = true; // by default black is 1
	boolean colorPlayed; //which color is playing next
	int turnNb;
	

	//initialize the chess board to first position
	public void initializeTable() 
	{
		for(int i = 0; i < 8; i++) 
		{
			for(int j = 0; j < 8; j++) 
			{
				if(i == 7) {
					switch(j) {
						case 0: 
							chessBoard[i][j] = 'R';
							break;
						case 1: 
							chessBoard[i][j] = 'N';
							break;
						case 2: 
							chessBoard[i][j] = 'B';
							break;
						case 3: 
							chessBoard[i][j] = 'Q';
							break;
						case 4: 
							chessBoard[i][j] = 'K';
							break;
						case 5: 
							chessBoard[i][j] = 'B';
							break;
						case 6: 
							chessBoard[i][j] = 'N';
							break;
						case 7: 
							chessBoard[i][j] = 'R';
							break;
					}
				}
				else if(i == 6) {
					chessBoard[i][j] = 'P';
				}
				else if(i == 1 ) {
					chessBoard[i][j] = 'p';
				}
				else if(i == 0) {
					switch(j) {
						case 0: 
							chessBoard[i][j] = 'r';
							break;
						case 1: 
							chessBoard[i][j] = 'n';
							break;
						case 2: 
							chessBoard[i][j] = 'b';
							break;
						case 3: 
							chessBoard[i][j] = 'q';
							break;
						case 4: 
							chessBoard[i][j] = 'k';
							break;
						case 5: 
							chessBoard[i][j] = 'b';
							break;
						case 6: 
							chessBoard[i][j] = 'n';
							break;
						case 7: 
							chessBoard[i][j] = 'r';
							break;
					}
				}
				else {
					chessBoard[i][j] = '-';
				}
			}
		}
	}


	//set the booleans of castling to false
	void checkRoque()
	{
		// if the black rook has moved set the boolean to false
		if(chessBoard[0][0] != 'r') blackRoqueQS = false;
		// if the black rook has moved set the boolean to false
		if(chessBoard[0][7] != 'r') blackRoqueKS = false;
		// if the black king has moved set the two booleans to false
		if(chessBoard[0][4] != 'k')
		{
			blackRoqueKS = false;
			blackRoqueQS = false;
		}
		// if the white rook has moved set boolean to false
		if(chessBoard[7][0] != 'R') whiteRoqueQS = false;
		// if the white rook has moved set boolean to false
		if(chessBoard[7][7] != 'R') whiteRoqueKS = false;
		// if the white king has moved set the two booleans to false
		if(chessBoard[7][4] != 'K')
		{
			whiteRoqueKS = false;
			whiteRoqueQS = false;
		}
	}
	
	//send a move and the chessboard will be updated
	public void updateTable(String move) 
	{
		
		//move is displayed	
		System.out.println(move);

		// cast to int the depature square 
		int depSquareLet = move.charAt(0) - 'a'; //letter 
		int depSquareNum = move.charAt(1) - '1'; //integer
		// cast to int the final square 
		int finSquareLet = move.charAt(2) - 'a'; //letter
		int finSquareNum = move.charAt(3) - '1'; //integer
		// get the piece of the departure square
		char piece = chessBoard[7 - depSquareNum][depSquareLet];
		
		//en passant temporar string set to NULL
		String enPassantTemp = "";
		//get the destination square of the move
		enPassantTemp += move.charAt(2);
		enPassantTemp += move.charAt(3);
		//if en passant move
		if(EnPassant.equals(enPassantTemp))
		{
			//if black
			if(piece == 'p')
			{
				//white pawn is taken
				chessBoard[7 - finSquareNum - 1][finSquareLet] = '-';
			} 
			//if white
			else if (piece == 'P')
			{
				//black pawn is taken
				chessBoard[7 - finSquareNum + 1][finSquareLet] = '-';	
			} 
		}

		//re-initialize en passant string
		EnPassant = "";
//execute roque
		if(piece == 'K' || piece == 'k')
		{
			if(depSquareLet - finSquareLet == -3) //roque king side for player
			{
				//moving king
				chessBoard[7 - depSquareNum][depSquareLet] = '-';
				chessBoard[7 - finSquareNum][finSquareLet - 1] = piece;
				//moving rook
				piece = chessBoard[7 - finSquareNum][finSquareLet];
				chessBoard[7 - finSquareNum][finSquareLet] = '-';
				chessBoard[7 - finSquareNum][finSquareLet - 2] = piece;
			}

			else if(depSquareLet - finSquareLet == -2) //roque king side for stockfish
			{
				//moving king
				chessBoard[7 - depSquareNum][depSquareLet] = '-';
				chessBoard[7 - finSquareNum][finSquareLet] = piece;
				//moving rook
				piece = chessBoard[7 - finSquareNum][finSquareLet + 1];
				chessBoard[7 - finSquareNum][finSquareLet + 1] = '-';
				chessBoard[7 - finSquareNum][finSquareLet - 1] = piece;
			}

			else if(depSquareLet - finSquareLet == 4) //roque queen side for player
			{
				//moving king
				chessBoard[7 - depSquareNum][depSquareLet] = '-';
				chessBoard[7 - finSquareNum][finSquareLet + 2] = piece;
				//moving rook
				piece = chessBoard[7 - finSquareNum][finSquareLet];
				chessBoard[7 - finSquareNum][finSquareLet] = '-';
				chessBoard[7 - finSquareNum][finSquareLet + 3] = piece;
			}

			else if(depSquareLet - finSquareLet == 2) //roque queen side for stockfish
			{
				//moving king
				chessBoard[7 - depSquareNum][depSquareLet] = '-';
				chessBoard[7 - finSquareNum][finSquareLet] = piece;
				//moving rook
				piece = chessBoard[7 - finSquareNum][finSquareLet - 2];
				chessBoard[7 - finSquareNum][finSquareLet - 2] = '-';
				chessBoard[7 - finSquareNum][finSquareLet + 1] = piece;
			}
			else
			{
				chessBoard[7 - depSquareNum][depSquareLet] = '-';
				chessBoard[7 - finSquareNum][finSquareLet] = piece;
			}

		}
		else
		{
			chessBoard[7 - depSquareNum][depSquareLet] = '-';
			chessBoard[7 - finSquareNum][finSquareLet] = piece;
		}
		//update castling boolean
		checkRoque();

		//if there is no promotion
		if(move.length() == 4) 
		{
			//updating en passant for the next move
			if(piece == 'p')
			{
				//if pawn has moved two squares
				if(depSquareNum - finSquareNum == 2) 
				{
					int temp = finSquareNum + 2;
					EnPassant += move.charAt(2);
					EnPassant += (char)(temp + '0');
				}
			}
			else if(piece == 'P')
			{
				//if pawn has moved two squares
				if(depSquareNum - finSquareNum == -2) 
				{
					int temp = finSquareNum;
					EnPassant += move.charAt(2);
					EnPassant += (char)(temp + '0');
				}
			}
		}
		// if there is a promotion
		else if(move.length() == 5) 
		{
			//get the promoted piece
			char promotion = move.charAt(4);
			//if black
			if(piece == 'p')
			{
				//promote to black queen
				if(promotion == 'q') chessBoard[7 - finSquareNum][finSquareLet] = promotion;
				//promote to black rook
				else if (promotion == 'r') chessBoard[7 - finSquareNum][finSquareLet] = promotion;
				//promote to black knight
				else if (promotion == 'n') chessBoard[7 - finSquareNum][finSquareLet] = promotion;
				//promote to black bishop
				else if (promotion == 'b') chessBoard[7 - finSquareNum][finSquareLet] = promotion;
			}
			//if white
			else if (piece == 'P')
			{
				//promote to white queen
				if(promotion == 'q') chessBoard[7 - finSquareNum][finSquareLet] = (char) (promotion - 'a' + 'A');
				//promote to white rook
				else if (promotion == 'r') chessBoard[7 - finSquareNum][finSquareLet] = (char) (promotion  - 'a' + 'A');
				//promote to white knight
				else if (promotion == 'n') chessBoard[7 - finSquareNum][finSquareLet] = (char) (promotion  - 'a' + 'A');
				//promote to white bishop
				else if (promotion == 'b') chessBoard[7 - finSquareNum][finSquareLet] = (char) (promotion - 'a' + 'A');
			}
		}
	}

	//set the game status in a file (normal, check, stalemate, checkmate, game over)
	void setStatus()
	{
		//launch a python file
		try {
			Runtime rt = Runtime.getRuntime();
			try {
				Process p =  rt.exec("python3 get_status.py");
				p.waitFor();
				} catch(InterruptedException e)
				{
	     			// nothing to do here
				}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		//open the file status (normal, check, stalemate, checkmate, game over)
		File f = new File("status.txt");
		// if file exists
		if(f.exists()) 
		{
			//read the file
			BufferedReader rd;
			try {
				rd = new BufferedReader(new FileReader("status.txt"));
				try {
					status = rd.readLine();
					rd.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		else 
		{
			System.out.println("File status doesn't exist");
		}
	}

	String getStatus()
	{
		String status="";
		File end = new File("status.txt");
		if(end.exists()) {
			BufferedReader rd;
			try {
				rd = new BufferedReader(new FileReader("status.txt"));
				try {
					status = rd.readLine();
					rd.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		else {
			System.out.println("File status.txt doesn't exist");
		}
		return status;
	}

	String getQuit()
	{
		String q = "";
		//verify quit status
		File qt = new File("quit.txt");
		if(qt.exists()) {
			BufferedReader rd;
			try {
				rd = new BufferedReader(new FileReader("quit.txt"));
				try {
					q = rd.readLine();
					rd.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		else {
			System.out.println("File quit.txt doesn't exist");
		}
		return q;
	}
	
	//check if player move is ok
	public boolean checkPlayerMove() 
	{
		String check;
		try {
			//launch a python file
			Runtime rt = Runtime.getRuntime();
			try{
			Process p = rt.exec("python3 check_move.py");
			p.waitFor();
			}
			catch(InterruptedException e)
			{
	     			// nothing to do here
			}
			
			//read the file
			BufferedReader rd;
			rd = new BufferedReader(new FileReader("moveOK.txt"));
			check = rd.readLine();
			rd.close();
			//if OK written in file move is ok
			if(check.equals("OK")) {
				return true;
			}
			//if NOK written in file move is not ok
			else {
				return false;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	public void move()
	{
		File f = new File("move.txt");
		String move = "";
		if(f.exists()) 
		{
			//check player move
			if(checkPlayerMove() == true) 
			{
				//read content of move 
				BufferedReader rd;
				try {
					
					rd = new BufferedReader(new FileReader("move.txt"));
					try {
						move = rd.readLine();
						rd.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				
				//execute move
				updateTable(move);
				//changing color
				if (colorPlayed == true) colorPlayed = false;
				else colorPlayed = true;
				//convert chess board in fen code
				convertTabInFenCode(colorPlayed,turnNb);

			}
			else System.out.println("Move isn't OK");
			//delete move.txt file
			f.delete();
		}
		else {
			System.out.println("File move.txt doesn't exist");
		}
	}

	//convert fen code in chessboard
	public void convertFenCodeInTab(String fen2)
	{
		String fen = "";
		if(fen2.equals(""))
		{
			
			//open file fen
			File f = new File("fen.txt");
			if(f.exists()) {
				//read fen file
				BufferedReader rd;
				try {
					rd = new BufferedReader(new FileReader("fen.txt"));
					try {
						//get fen
						String sCurrentLine = "";
					    while ((sCurrentLine = rd.readLine()) != null) 
					    {
					        System.out.println(sCurrentLine);
					        fen = sCurrentLine;
					    }					
					    rd.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
			else {
				System.out.println("File fen.txt doesn't exist");
			}
		}
		else 
		{
			fen = fen2;
		}

		
		int size = fen.length();
		int tmpsize = 0;
		if(size > 0)
		{
			for (int i = 0 ; i < 8 ; i++)
			{
				for (int j = 0 ; j < 8 ; j++)
				{
					if(fen.charAt(tmpsize) == '8')// && fen.charAt(tmpsize) <= '8') 
					{
						int compt = 0;
						while(compt < 8)
						{
							chessBoard[i][j] = '-';		
							if(j < 8)j++;
							compt++;
						}
						j--;
					}					
					else if(fen.charAt(tmpsize) == '7')// && fen.charAt(tmpsize) <= '8') 
					{
						int compt = 0;
						while(compt < 7)
						{
							chessBoard[i][j] = '-';		
							if(j < 8)j++;
							compt++;
						}
						j--;
					}
					else if(fen.charAt(tmpsize) == '6')// && fen.charAt(tmpsize) <= '8') 
					{
						int compt = 0;
						while(compt < 6)
						{
							chessBoard[i][j] = '-';		
							if(j < 8)j++;
							compt++;
						}
						j--;
					}
					else if(fen.charAt(tmpsize) == '5')// && fen.charAt(tmpsize) <= '8') 
					{
						int compt = 0;
						while(compt < 5)
						{
							chessBoard[i][j] = '-';		
							if(j < 8)j++;
							compt++;
						}
						j--;
					}
					else if(fen.charAt(tmpsize) == '4')// && fen.charAt(tmpsize) <= '8') 
					{
						int compt = 0;
						while(compt < 4)
						{
							chessBoard[i][j] = '-';		
							if(j < 8)j++;
							compt++;
						}
						j--;
					}
					else if(fen.charAt(tmpsize) == '3')// && fen.charAt(tmpsize) <= '8') 
					{
						int compt = 0;
						while(compt < 3)
						{
							chessBoard[i][j] = '-';		
							if(j < 8)j++;
							compt++;
						}
						j--;
					}
					else if(fen.charAt(tmpsize) == '2')// && fen.charAt(tmpsize) <= '8') 
					{
						int compt = 0;
						while(compt < 2)
						{
							chessBoard[i][j] = '-';		
							if(j < 8)j++;
							compt++;
						}
						j--;
					}
					else if(fen.charAt(tmpsize) == '1')// && fen.charAt(tmpsize) <= '8') 
					{
						int compt = 0;
						while(compt < 1)
						{
							chessBoard[i][j] = '-';		
							if(j < 8)j++;
							compt++;
						}
						j--;
					}
					else if(fen.charAt(tmpsize) >= 'a' && fen.charAt(tmpsize) <= 'z') 
					{
						chessBoard[i][j] = fen.charAt(tmpsize);
					}
					else if(fen.charAt(tmpsize) >= 'A' && fen.charAt(tmpsize) <= 'Z') 
					{
						chessBoard[i][j] = fen.charAt(tmpsize);
					}
					else if(fen.charAt(tmpsize) == '/')
					{
						j--;
					} 
					else if(fen.charAt(tmpsize) == ' ')
					{
						tmpsize++;
					} 
					if(tmpsize < size) tmpsize++;
				}
			}
		}

		//variable init
		EnPassant = "";
		whiteRoqueQS = false;
		blackRoqueQS = false;
		whiteRoqueKS = false;
		blackRoqueKS = false;

		System.out.println(fen.charAt(tmpsize));

		if(tmpsize <= size)
		{
			tmpsize++; //space
					System.out.println(fen.charAt(tmpsize));

			if(fen.charAt(tmpsize) == 'w') colorPlayed = false;
			else colorPlayed = true;
			
			tmpsize++; //space
			tmpsize++; 
			if(fen.charAt(tmpsize) != '-')
			{
				while(fen.charAt(tmpsize) != ' ')
				{
					if(fen.charAt(tmpsize) == 'K')
					{
						whiteRoqueKS = true;
					}
					else if(fen.charAt(tmpsize) == 'Q')
					{
						whiteRoqueQS = true;
					}
					else if(fen.charAt(tmpsize) == 'k')
					{
						blackRoqueKS = true;
					}
					else if(fen.charAt(tmpsize) == 'q')
					{
						blackRoqueQS = true;
					}
					tmpsize++;
				}
			}

			tmpsize++; //space
			if(fen.charAt(tmpsize) != '-')
			{
				EnPassant += fen.charAt(tmpsize);
				tmpsize++;
				EnPassant += fen.charAt(tmpsize);
			}
			tmpsize++;
		}
	}

	//convert chessboard in fen code
	public void convertTabInFenCode(boolean nextPlayer, int comptCoup) 
	{
		//initialize string
		String fen = "";
		//fill the fen string with the chess board
		int compt;
		//double for loop
		for (int i = 0 ; i < 8; i++)
		{
			compt = 0;
			for (int j = 0 ; j < 8; j++)
			{
				if(chessBoard[i][j] != '-') 
				{
					if(compt > 0) 
					{
						char temp = (char)(compt + '0');
						fen += temp;
					}
					fen += chessBoard[i][j];
					compt = 0;
				}
				else 
				{
					compt ++;
				}
				if(j == 7 && compt > 0) 
				{
					char temp = (char)(compt + '0');
					fen += temp;
				}
			}
			if(i < 7) fen += "/";
		}

		//next color playing
		fen += " ";
		if(nextPlayer == white) fen += "w"; //white moving next
		else fen += "b"; //black moving next

		//add castling booleans
		fen += " ";
		if(whiteRoqueKS)fen += "K"; //white can castle king side
		if(whiteRoqueQS)fen += "Q"; //white can castle queen side
		if(blackRoqueKS)fen += "k"; //black can castle king side
		if(blackRoqueQS)fen += "q"; //black can castle queen side
		if( !whiteRoqueKS && !whiteRoqueQS && ! blackRoqueKS && !blackRoqueQS) fen += "-"; //no castling
		
		//add en passant destination square
		fen += " ";
		if(EnPassant.length() > 0) fen += EnPassant; //en passant
		else fen += "-"; //no en passant

		//add 0 
	   	fen += " 0 0";
	   	// add number of round
	   	//char temp = (char)(comptCoup + '0');
	   	//fen += temp;

	   	//write in fen file
	   	File f = new File("fen.txt");
		if(f.exists()) {
				try {
					FileWriter writer = new FileWriter("fen.txt",true);
					writer.write(fen);
					writer.write("\n");
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		else {
			System.out.println("File fen doesn't exist");
		}
	}
	
	//recover ia move from a file 
	public void recoverIAmove() 
	{
		//string initialized
		String move = "";
		try {
			//run python to write ai bestmove in a file			
			Runtime rt = Runtime.getRuntime();
			try {
			Process p = rt.exec("python3 get_iamove.py");
			p.waitFor(); 
			} catch(InterruptedException e)
			{
	     			// nothing to do here
			}	
		} catch (IOException e) {
			e.printStackTrace();
		}

		//open file bestmove
		File f = new File("bestmove.txt");
		if(f.exists()) {
			//read bestmove file
			BufferedReader rd;
			try {
				rd = new BufferedReader(new FileReader("bestmove.txt"));
				try {
					//get bestmove
					move = rd.readLine();
					rd.close();
					//update chessboard with ai bestmove
					updateTable(move);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			//delete bestmove file
			f.delete();
		}
		else {
			System.out.println("File bestmove.txt doesn't exist");
		}
	}

	//print the chessboard
	public void affichage()
	{
	    int i,j;
	    
	    System.out.flush();
	    System.out.println("\n_________________________________");
	    for (i=0; i<8; i++)
	    {
	        for(j=0; j<8; j++)
	        {
	        	if (j== 0) System.out.print("| ");
	            else System.out.print(" | ");
	        	System.out.print(chessBoard[i][j]);
	        }
	        System.out.println(" |");
	        System.out.println("_________________________________");
	    }	    
	}
	
	//game function for player vs ia 
	public int game(boolean color, boolean first) 
	{
		//initialize game parameters if first game
		d = new DisplayBoard();	
		//ai color is not player color
		boolean IAcolor = !color;
		//win is initialized to false
		boolean win = false;
		//array of taken pieces
		char blackPieceTaken[] = new char[16];
		//array of taken pieces
		char whitePieceTaken[] = new char[16];
		//number of round played
		int turnNb = 1;
		//init move
		String move = "";
		//int en passant
		EnPassant = "";
		//init taken piece array
		for(int i = 0; i < 16; i++) {
			blackPieceTaken[i] = ' ';
			whitePieceTaken[i] = ' ';
		}
	
		if (first == false)
		{		
			File file = new File("fenia.txt");
			File file2 = new File("fen.txt");
			file.renameTo(file2);
			convertFenCodeInTab("");
		}
		else
		{
			//first player playing
			colorPlayed = white;
			initializeTable();
			File fen = new File("fen.txt");
			try 
			{
				fen.createNewFile();
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
			File jb= new File("fenia.txt");
			if(jb.exists())
			{
				jb.delete();
			}
			//convert chess board in fen code
			convertTabInFenCode(colorPlayed,turnNb);
		}

		//print chessboard
		affichage();
		d.PrintChessBoard(chessBoard,true,false);
		

		//while loop (game loop)
		do {
			//verify game status
			setStatus();
			String status = getStatus();
			if(status.equals("CHECK"))
			{
				System.out.println("Vous etes en echec");
				d.displayStatus("check");
			}
			else if(status.equals("STALEMATE")) 
			{
				d.displayStatus("stalemate");
				try {
	       		    Thread.sleep(10000) ;
		        }  catch (InterruptedException e) {
		            // gestion de l'erreur
		        }
				d.close();
				return 2;
			}
			else if(status.equals("GAMEOVER"))
			{
				d.displayStatus("gameover");
				try {
		            Thread.sleep(10000) ;
		         }  catch (InterruptedException e) {
		             // gestion de l'erreur
		         }
				d.close();
				return 2;
			}
			else if(status.equals("CHECKMATE")) 
			{
				d.displayStatus("checkmate");
				try {
		       	    Thread.sleep(10000) ;
		        }  catch (InterruptedException e) {  
		            // gestion de l'erreur
		        }
				d.close();
				return (!colorPlayed ? 1 : 0);
			}
			else if(status.equals("NORMAL")) 
			{
				d.displayStatus("normal");
			}

			//if ia is playing
			if(colorPlayed == white && IAcolor == white) 
			{

				//d.displayPlayer(false, false);

				//get and execute bestmove
				recoverIAmove();
				//changing color
				if (colorPlayed == true) colorPlayed = false;
				else colorPlayed = true;
				//convert chess board in fen code
				convertTabInFenCode(colorPlayed,turnNb);
			}
			//if player is playing
			else if(colorPlayed == white && IAcolor == black) 
			{
				//d.displayPlayer(true, true);

				//speech recognizer
				SpeechRecognizer sc = new SpeechRecognizer();
				sc.SpeechRecognizerLaunch("Game",d,null);	
				
				//while move.txt or quit.txt does not exists
				boolean test = true;
				while(test)
				{
					//check existence of move.txt
					File mv = new File("move.txt");
					if(mv.exists()) test = false;
					File qt = new File("quit.txt");
					if(qt.exists()) test = false;
				}

				String q = "";
				q = getQuit();
				System.out.println(q);
				if(q.equals("menu"))
				{
					d.close();
					return 3;
				}

				move();
			}
			//if player is playing
			else if(colorPlayed == black && IAcolor == white) 
			{

				//d.displayPlayer(true, true);

				//speech recognizer
				SpeechRecognizer sc = new SpeechRecognizer();
				sc.SpeechRecognizerLaunch("Game",d,null);
				
				//while move.txt or quit.txt does not exists
				boolean test = true;
				while(test)
				{
					//check existence of move.txt
					File mv = new File("move.txt");
					if(mv.exists()) test = false;
					File qt = new File("quit.txt");
					if(qt.exists()) test = false;
				}

				String q = getQuit();
				if(q.equals("menu"))
				{
					d.close();
					return 3;
				}

				move();
			}
			//if ia is playing
			else if(colorPlayed == black && IAcolor == black) 
			{

				//d.displayPlayer(false, false);

				//check and execute ia move
				recoverIAmove();
				//changing color
				if (colorPlayed == true) colorPlayed = false;
				else colorPlayed = true;
				//convert chess board in fen code	
				convertTabInFenCode(colorPlayed,turnNb);
			}
			//display chess board
			affichage();
			d.PrintChessBoard(chessBoard,!(colorPlayed),false);

		} while(win != true);
		return 3;
	}

	//game function for player vs player  
	public int gamePvP(boolean color, boolean first) 
	{

		//initialize game parameters if first game
		d = new DisplayBoard();	
		//win is initialized to false
		boolean win = false;
		
		//array of taken pieces
		char blackPieceTaken[] = new char[16];
		//array of taken pieces
		char whitePieceTaken[] = new char[16];
		//number of round played
		int turnNb = 1;
		//init move
		String move = "";
		//init en passant
		EnPassant = "";
		//init taken piece array
		for(int i = 0; i < 16; i++) {
			blackPieceTaken[i] = ' ';
			whitePieceTaken[i] = ' ';
		}

		if (first == false)
		{		
			File file = new File("fenpvp.txt");
			File file2 = new File("fen.txt");
			file.renameTo(file2);
			convertFenCodeInTab("");
		}
		else
		{
			//first player playing
			colorPlayed = white;
			initializeTable();
			File fen = new File("fen.txt");
			try 
			{
				fen.createNewFile();
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
			File jb = new File("fenpvp.txt");
			if(jb.exists())
			{
				jb.delete();
			}
			//convert chess board in fen code
			convertTabInFenCode(colorPlayed,turnNb);
		}

		//print chessboard
		affichage();
		d.PrintChessBoard(chessBoard,colorPlayed,true);

		//while loop (game loop)
		do {

			//verify game status
			setStatus();
			String status = getStatus();
			if(status.equals("CHECK"))
			{
				System.out.println("Vous etes en echec");
				d.displayStatus("check");
			}
			else if(status.equals("STALEMATE")) 
			{
				d.displayStatus("stalemate");
				try {
	       		    Thread.sleep(10000) ;
		        }  catch (InterruptedException e) {
		            // gestion de l'erreur
		        }
				d.close();
				return 2;
			}
			else if(status.equals("GAMEOVER"))
			{
				d.displayStatus("gameover");
				try {
		            Thread.sleep(10000) ;
		         }  catch (InterruptedException e) {
		             // gestion de l'erreur
		         }
				d.close();
				return 2;
			}
			else if(status.equals("CHECKMATE")) 
			{
				d.displayStatus("checkmate");
				try {
		       	    Thread.sleep(10000) ;
		        }  catch (InterruptedException e) {  
		            // gestion de l'erreur
		        }
				d.close();
				return (!colorPlayed ? 1 : 0);
			}
			else if(status.equals("NORMAL")) 
			{
				d.displayStatus("normal");
			}

			//if first player is playing
			if(colorPlayed == white && color == white) 
			{

				//d.displayPlayer(true, true);

				//speech recognizer
				SpeechRecognizer sc = new SpeechRecognizer();
				sc.SpeechRecognizerLaunch("Game",d,null);
				//while move.txt or quit.txt does not exists
				boolean test = true;
				while(test)
				{
					//check existence of move.txt
					File mv = new File("move.txt");
					if(mv.exists()) test = false;
					File qt = new File("quit.txt");
					if(qt.exists()) test = false;
				}

				String q = "";
				q = getQuit();
				if(q.equals("menu"))
				{
					d.close();
					return 3;
				}

				move();
			}

			//if second player is playing
			else
			{

				//d.displayPlayer(true, true);

				//speech recognizer
				SpeechRecognizer sc = new SpeechRecognizer();
				sc.SpeechRecognizerLaunch("Game",d,null);
				
				//while move.txt or quit.txt does not exists
				boolean test = true;
				while(test)
				{
					//check existence of move.txt
					File mv = new File("move.txt");
					if(mv.exists()) test = false;
					File qt = new File("quit.txt");
					if(qt.exists()) test = false;
				}

				String q = "";
				q = getQuit();
				if(q.equals("menu"))
				{
					d.close();
					return 3;
				}

				move();
			}
			
			//display chess board
			affichage();
			d.PrintChessBoard(chessBoard,colorPlayed,true);

		} while(win != true);

		return 3;
	}

	public void replay() {
        d = new DisplayBoard();
		String fen = "";
        BufferedReader rd;
        String sCurrentLine = "";
        try {
            rd = new BufferedReader(new FileReader("fenia.txt"));
            try {
                //get fen code line by line
                while ((sCurrentLine = rd.readLine()) != null) {
                    fen = sCurrentLine;
                    convertFenCodeInTab(fen);
                    d.PrintChessBoard(chessBoard,false,false);
                    d.displayNone();
                    try {
                        Thread.sleep(5000); // wait for 5 s to see the play
                    } catch(InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                rd.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        d.close();
    }
}

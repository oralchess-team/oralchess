
import chess.uci
import chess

f = open("fen.txt","r") 
last_line = f.readlines()[-1]
f.close()

engine = chess.uci.popen_engine("stockfish")
engine.uci()
board = chess.Board(last_line)
print(board)
engine.position(board)
tmp = str(engine.go(movetime=2000))
move = tmp.split("'")
print(move[1])
engine.quit()

f = open("bestmove.txt","w")
f.write(move[1])
f.close()

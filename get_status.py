
import chess

f = open("fen.txt","r")
last_line = f.readlines()[-1]
f.close()

board = chess.Board(last_line)
print(board)

f = open("status.txt","w")

if board.is_checkmate() == True:
	print ("CHECKMATE")
	f.write("CHECKMATE")
elif board.is_check() == True:
	print ("CHECK")
	f.write("CHECK")
elif board.is_stalemate() == True:
	print ("STALEMATE")
	f.write("STALEMATE")
elif board.is_game_over() == True:
	print ("GAMEOVER")
	f.write("GAMEOVER")
else:
	print ("NORMAL")
	f.write("NORMAL")
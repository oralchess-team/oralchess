
/*import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import java.awt.image.BufferedImage;

import java.io.File;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import javax.imageio.ImageIO;

/*public class DisplayMenu extends JPanel {

    private final JPanel gui = new JPanel(new BorderLayout(3, 3));
    JFrame f = new JFrame();

    DisplayMenu() {
        f.add(this.getGui());
        f.setTitle("OralChess Board");
        f.setResizable(false);
        f.setBackground(new Color(100, 100, 100));
        f.setSize(1280, 800);
        f.setLocation(0, 0);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        initMenuImage();
    }

    public void initMenuImage()
    {

    }

    public void paint(Graphics graphics) {
        ImageIcon menuBackground = new ImageIcon("Image/menuWallpaper.png");
        Font font = new Font("Century Gothic", Font.BOLD, 20);
        graphics.drawImage(menuBackground.getImage(), 0, 0, null);
        graphics.setFont(font);
        graphics.setColor(Color.white);
        graphics.drawString("Say the number corresponding to the chosen option:", 10, 40);
        graphics.drawString("1. Player versus Stockfish IA", 10, 70);
        graphics.drawString("2. Player versus Player", 10, 100);
        graphics.drawString("3. Load player versus Stockfish IA party", 10, 130);
        graphics.drawString("4. Load player versus player party", 10, 160);
        graphics.drawString("5. Game rules", 10, 190);
    }


    public void repaint(Graphics graphics) {
        paint(graphics);
    }

    public void printInfo(String info) {
        Graphics graphics = getGraphics();
        Font font = new Font("Century Gothic", Font.BOLD, 20);
        graphics.setFont(font);
        graphics.setColor(Color.white);
        graphics.drawString(info, 500, 700);
    }


    public void rules() {
        String info1 = "For playing at this game, you have to use your voice to tell the game your order.\n";
        String info2 = "When you are in the Menu, you just have to say a number between one and seven.\n";
        String info3 = "During the game,a led in the game inform you when it's your turn. It depends on the led: if led is green you can speak if it's red it's not.\n";
        String info4 = "So when it's your turn, you have to tell the game 4 numbers : each number is comprised between and 8.\n";
        String info5 = "But there is some particular command : if you want to go back to the menu you have to say menu.\n";
        String info6 = "Moreover when you want to do a castling, youn have to say the movement of the king on the place where the rook is.\n";
        String info7 = "If you can promote a pawn, you have to say in addition of the move: queen, rook, bishop or knight to have what you want.\n";
        Graphics graphics = getGraphics();
        Font font = new Font("Century Gothic", Font.BOLD, 20);
        graphics.setFont(font);
        graphics.setColor(Color.white);
        graphics.drawString(info1, 100, 100);
        graphics.drawString(info2, 100, 150);
        graphics.drawString(info3, 100, 200);
        graphics.drawString(info4, 100, 250);
        graphics.drawString(info5, 100, 300);
        graphics.drawString(info6, 100, 350);
        graphics.drawString(info7, 100, 400);
    }


    public void printRecording(boolean isRecording) {
        Graphics g = getGraphics();
    if(isRecording) {
            g.setColor(Color.green);
            g.fillOval(950, 120, 50, 50);
        } else {
            g.setColor(Color.red);
            g.fillOval(950, 120, 50, 50);
        }
    }

    public void close() {
        menuWindow.dispose();
    }
}*/


import java.awt.*;
import java.awt.image.BufferedImage;
import javax.swing.*;
import javax.swing.border.*;
import javax.imageio.ImageIO;
import java.io.File;
import java.awt.Color;

public class DisplayMenu {

    private final JPanel gui = new JPanel(new BorderLayout(3, 3));
    private JButton[] menuChoice = new JButton[7];
    private JButton[] rules = new JButton[7];
    private JPanel menu;
    JFrame f = new JFrame("OralChess");
    JButton record,iaplay,texte,hello,logo;


    public DisplayMenu() {
        f.add(this.getGui());
        f.setTitle("OralChess Menu");
        f.setResizable(false);
        f.setBackground(new Color(100, 100, 100));
        f.setSize(1280, 800);
        f.setLocation(0, 0);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        initializeDisplayMenu();
    }

    public final void initializeDisplayMenu() {
        
// set up the main GUI
        
        Color gr = new Color(0,100,0);
        gui.setBackground(gr);
        BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);

        // Create a new blank cursor.
        Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
        cursorImg, new Point(0, 0), "blank cursor");

        // Set the blank cursor to the JFrame.
        f.getContentPane().setCursor(blankCursor);

        gui.setLayout(null);
        menuChoice[0] = new JButton("1. Player versus Stockfish IA");
        menuChoice[0].setBounds(340,200,600,45);
        menuChoice[0].setVisible(false);
        gui.add(menuChoice[0]);
        menuChoice[1] = new JButton("2. Player versus Player");
        menuChoice[1].setBounds(340,250,600,45);
        menuChoice[1].setVisible(false);
        gui.add(menuChoice[1]);
        menuChoice[2] = new JButton("3. Load player versus Stockfish IA game");
        menuChoice[2].setBounds(340,300,600,45);
        menuChoice[2].setVisible(false);
        gui.add(menuChoice[2]);
        menuChoice[3] = new JButton("4. Load player versus player game");
        menuChoice[3].setBounds(340,350,600,45);
        menuChoice[3].setVisible(false);
        gui.add(menuChoice[3]);
        menuChoice[4] = new JButton("5. Replay");
        menuChoice[4].setBounds(340,400,600,45);
        menuChoice[4].setVisible(false);
        gui.add(menuChoice[4]);
        menuChoice[5] = new JButton("6. Game rules");
        menuChoice[5].setBounds(340,450,600,45);
        menuChoice[5].setVisible(false);
        gui.add(menuChoice[5]);
        menuChoice[6] = new JButton("7. Quit");
        menuChoice[6].setBounds(340,500,600,45);
        menuChoice[6].setVisible(false);
        gui.add(menuChoice[6]);
        String info1 = "To play this game, you must use your voice to tell the game your order.";
        String info2 = "When you are in the Menu, you just have to say a number between one and seven.";
        String info3 = "During the game and in the menu, a green led informs you when you can speak. It turns red when deepspeech processes.";
        String info4 = "When it's your turn, you have to tell the game your departure square and your destination square (ex : 1-2 1-4).";
        String info5 = "There are some particular command : if you want to return to the menu you have to say BACK.";
        String info6 = "Moreover when you want to castle, you have to say the square of the king followed by the square of the rook.";
        String info7 = "To promote a pawn you have to say, in addition of the move: QUEEN, ROOK, BISHOP or KNIGHT (ex : 1-7 1-8 Queen).";
        rules[0] = new JButton(info1);
        rules[0].setBounds(140,200,950,45);
        rules[0].setVisible(false);
        gui.add(rules[0]);
        rules[1] = new JButton(info2);
        rules[1].setBounds(140,250,950,45);
        rules[1].setVisible(false);
        gui.add(rules[1]);
        rules[2] = new JButton(info3);
        rules[2].setBounds(140,300,950,45);
        rules[2].setVisible(false);
        gui.add(rules[2]);
        rules[3] = new JButton(info4);
        rules[3].setBounds(140,350,950,45);
        rules[3].setVisible(false);
        gui.add(rules[3]);
        rules[4] = new JButton(info5);
        rules[4].setBounds(140,400,950,45);
        rules[4].setVisible(false);
        gui.add(rules[4]);
        rules[5] = new JButton(info6);
        rules[5].setBounds(140,450,950,45);
        rules[5].setVisible(false);
        gui.add(rules[5]);
        rules[6] = new JButton(info7);
        rules[6].setBounds(140,500,950,45);
        rules[6].setVisible(false);
        gui.add(rules[6]);
        texte = new JButton("Tell the game the number of your choice");
        texte.setBounds(440,15,400,25);
        texte.setVisible(false);
        gui.add(texte);
        record = new JButton();
        record.setBounds(1075,350,50,45);
        record.setBackground(Color.RED);
        record.setVisible(false);
        gui.add(record);
        iaplay = new JButton("Loading");
        iaplay.setBounds(1025,400,150,25);
        iaplay.setVisible(false);
        gui.add(iaplay);
        hello = new JButton("Your choice is not available because you have no saved game");
        hello.setBounds(300,600,680,25);
        hello.setVisible(false);
        gui.add(hello);
        logo = new JButton();
        logo.setBounds(120,325,100,100);
        logo.setVisible(false);
        try {
            BufferedImage bi = ImageIO.read(new File("Images/logo.png"));
            Icon icon = new ImageIcon(bi);
            logo.setIcon(icon);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        gui.add(logo);


        
        for(int i=0 ; i<7 ; i++)
        {
            rules[i].setVisible(false);
        }

        for(int i=0 ; i<7 ; i++)
        {
            menuChoice[i].setVisible(true);
        }
        record.setVisible(true);
        texte.setVisible(true);
        iaplay.setVisible(false);
        hello.setVisible(false);
        logo.setVisible(true);
    }

    public final JComponent getGui() {
        return gui;
    }

    /**
    * Indicate if the user can speak or not
    */
    public void displayRecording(boolean isRecording) {
        if(isRecording) {
                record.setBackground(Color.GREEN);
            } else {
                record.setBackground(Color.RED);
            }
    }

    public void displayLoading(boolean isLoading)
    {
        if(isLoading) {
            iaplay.setVisible(true);
        } else {
            iaplay.setVisible(false);
        }   
    }

    public void displayInfo(boolean display)
    {
        if(display) {
            hello.setVisible(true);
        } else {
            hello.setVisible(false);
        }   
    }

    public void displayRules() {
        for(int i=0 ; i<7 ; i++)
        {
            menuChoice[i].setVisible(false);
        }
        hello.setVisible(false);
        iaplay.setVisible(false);
        record.setVisible(false);
        logo.setVisible(false);
        texte.setVisible(true);
        texte.setText("RULES");
        for(int i=0 ; i<7 ; i++)
        {
            rules[i].setVisible(true);
        }
    }

      public void unDisplayRules() {
        for(int i=0 ; i<7 ; i++)
        {
            rules[i].setVisible(false);
            menuChoice[i].setVisible(true);
        }
        hello.setVisible(false);
        iaplay.setVisible(false);
        record.setVisible(true);
        texte.setVisible(true);
        logo.setVisible(true);
        texte.setText("Tell the game the number of your choice");
    }

    public void close()
    {
        f.dispose();
    }
}
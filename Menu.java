
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import java.lang.InterruptedException;

public class Menu {

	private DisplayMenu dm = new DisplayMenu();
	private GameLoop gl = new GameLoop();

	/**
	* Constructor
	*/
	public Menu() {
		boolean endMenu = false;
		while(!endMenu) {
			try {
	       		    Thread.sleep(4000) ;
		        }  catch (InterruptedException e) {
		            // gestion de l'erreur
		        }
			SpeechRecognizer sc = new SpeechRecognizer();
            sc.SpeechRecognizerLaunch("Menu",null,dm);
			switch(getMenuOption()) {
				case '1':
					launchGame("fenia.txt", true);
					break;
				case '2':
					launchGame("fenpvp.txt", true);
					break;
				case '3':
					checkAndLaunchIfPartyLoad("fenia.txt");
					break;
				case '4':
					checkAndLaunchIfPartyLoad("fenpvp.txt");
					break;
				case '5':
					checkAndLaunchReplay();
					break;
				case '6':
					manageRules();
					break;
				case '7':
					endMenu = true;
					break;
				default:
					break;
			}
		}
		dm.close();
	}

	/**
	* Get the option chosen by the user
	*/
	public char getMenuOption() {
		String menuFileName = "menu.txt";
		File menuFile = new File(menuFileName);
		char menuOption = 'a';
		boolean whilePassed = false;
		do {
			if(menuFile.exists()) {
				BufferedReader rd;
				try {
					rd = new BufferedReader(new FileReader(menuFileName));
					menuOption = rd.readLine().charAt(0);
					System.out.println(menuOption);
					rd.close();
				} catch(IOException e) {
					e.printStackTrace();
				}
				whilePassed = true;
			}
			
		} while(!whilePassed);
		menuFile.delete();
		return menuOption;
	}

	/**
	* Check if the save exists and then launch the game
	*/
	public void checkAndLaunchIfPartyLoad(String saveFileName) {
		File f = new File(saveFileName);
		if(f.exists()) {
			launchGame(saveFileName, false);
		} else {
			dm.displayInfo(true);
			try {
				Thread.sleep(5000);
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
			dm.displayInfo(false);
		}
	}

	/**
	* Check if the save exists and then launch the game
	*/
	public void checkAndLaunchReplay() {
		File f = new File("fenia.txt");
		if(f.exists()) {
			gl.replay();
		} else {
			dm.displayInfo(true);
			try {
				Thread.sleep(5000);
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
			dm.displayInfo(false);
		}
	}

	/**
	* Launch different types of game (PvP, PvIA) saved or not
	*/
	public void launchGame(String saveFileName, boolean partyStart) {
		int winner = 3;
		String gameFileName = "fen.txt";
		if(saveFileName.equals("fenia.txt")) {
			winner = gl.game(false, partyStart);
		} else if(saveFileName.equals("fenpvp.txt")) {
			winner = gl.gamePvP(false, partyStart);
		}
		if(winner == 2) {
			System.out.println("PAT");
			File gameFile = new File(gameFileName);
			File saveFile = new File(saveFileName);
			gameFile.renameTo(saveFile);
		} else if(winner == 0) {
			System.out.println("WHITE WIN");
			File gameFile = new File(gameFileName);
			File saveFile = new File(saveFileName);
			gameFile.renameTo(saveFile);
		} else if(winner == 1) {
			System.out.println("BLACK WIN");
			File gameFile = new File(gameFileName);
			File saveFile = new File(saveFileName);
			gameFile.renameTo(saveFile);
		} else if(winner == 3) {
			File gameFile = new File(gameFileName);
			File saveFile = new File(saveFileName);
			gameFile.renameTo(saveFile);
		}
		removeTemporaryFiles();
	}

	/**
	* Remove useless files
	*/
	public void removeTemporaryFiles() {
		File temporaryFile = new File("fen.txt");
		temporaryFile.delete();
		temporaryFile = new File("moveOK.txt");
		temporaryFile.delete();
		temporaryFile = new File("status.txt");
		temporaryFile.delete();
		temporaryFile = new File("bestmove.txt");
		temporaryFile.delete();
		temporaryFile = new File("quit.txt");
		temporaryFile.delete();
	}

	public void manageRules()
    {
        dm.displayRules();
        try {
				Thread.sleep(20000);
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
        dm.unDisplayRules();
    }

	/**
	* Main function to start the menu
	*/
	public static void main(String[] args) {
		new Menu();
		try {
			Runtime rt = Runtime.getRuntime();
			rt.exec("sudo shutdown now");
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
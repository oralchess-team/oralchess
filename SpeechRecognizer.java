
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.PrintWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;

import java.lang.Runtime;
import java.lang.Process;

public class SpeechRecognizer {

	private String premoveFileName = "premove.txt";
	private File premoveFile = new File(premoveFileName);
	private String audioFileName = "audioRecordFile.wav";
	private File audioFile = new File(audioFileName);

	/**
	* Call the speech-to-text recognizer DeepSpeech for RPi
	*/
	public void callDeepSpeech(String wavFileName,DisplayBoard d) {
		String launchDeepSpeech = "./deepspeech --model Models/output_graph.pbmm --alphabet Models/alphabet.txt --trie Models/trie --audio " + wavFileName;
		Process p;
		InputStream in;
		InputStreamReader isr;
		BufferedReader br;
		FileWriter fr;
		String gathered;
		String gathered2 = "";
		try {
			p = Runtime.getRuntime().exec(launchDeepSpeech);
			in = p.getInputStream();
			isr = new InputStreamReader(in);
			br = new BufferedReader(isr);
			while((gathered = br.readLine()) != null) {
				if(gathered != null) {
					gathered2 = gathered;
				}
			}
			if(d != null)
			{
				d.displayMove(gathered2);
			}
			System.out.println(gathered2);
			if(premoveFile.exists()) System.out.println("Il existe");
			fr = new FileWriter(premoveFile);
			fr.write(gathered2);
			fr.close();
			audioFile.delete();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

	/**
	* Get the result in the file
	*/
	public String obtainResultFromDeepSpeech() {
		String resultDeepSpeech = "";
		BufferedReader rd;
		try {
			rd = new BufferedReader(new FileReader(premoveFileName));
			resultDeepSpeech = rd.readLine();
			rd.close();
			premoveFile.delete();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return resultDeepSpeech;
	}

	/**
	* Call functions that verify the validity of the command in a party
	*/
	public String analyzeResultFromDeepSpeechInParty(String resultDeepSpeech) {
		if(resultDeepSpeech ==null) return "WrongCommand";
		String []resultDeepSpeechSplitted = resultDeepSpeech.split(" ");
		String finalResult = "";
		if(resultDeepSpeechSplitted.length == 1 || resultDeepSpeechSplitted.length == 4 || resultDeepSpeechSplitted.length == 5) {
			if(resultDeepSpeechSplitted.length == 1) {
				if(resultDeepSpeechSplitted[0].equals("back") || resultDeepSpeechSplitted[0].equals("but") || resultDeepSpeechSplitted[0].equals("bhut")) {
					return "menu";
				} else {
					return "WrongCommand";
				}
			} else if(resultDeepSpeechSplitted.length == 4) {
				finalResult += translateNumberInLetter(checkNumberForParty(resultDeepSpeechSplitted[0]));
				finalResult += checkNumberForParty(resultDeepSpeechSplitted[1]);
				finalResult += translateNumberInLetter(checkNumberForParty(resultDeepSpeechSplitted[2]));
				finalResult += checkNumberForParty(resultDeepSpeechSplitted[3]);
			} else {
				finalResult += translateNumberInLetter(checkNumberForParty(resultDeepSpeechSplitted[0]));
				finalResult += checkNumberForParty(resultDeepSpeechSplitted[1]);
				finalResult += translateNumberInLetter(checkNumberForParty(resultDeepSpeechSplitted[2]));
				finalResult += checkNumberForParty(resultDeepSpeechSplitted[3]);
				finalResult += checkPromoteWord(resultDeepSpeechSplitted[4]);
			}
			if(finalResult.contains("WrongCommand")) {
				return "WrongCommand";
			} else {
				return finalResult;
			}
		} else {
			return "WrongCommand";
		}
	}

	/**
	* Call functions that verify the validity of the command in a menu
	*/
	public String analyzeResultFromDeepSpeechInMenu(String resultDeepSpeech) {
		if(resultDeepSpeech ==null) return "WrongCommand";
		String []resultDeepSpeechSplitted = resultDeepSpeech.split(" ");
		String finalResult = "";
		if(resultDeepSpeechSplitted.length == 1) {
			finalResult = checkNumberForMenu(resultDeepSpeechSplitted[0]);
			return finalResult;
		} else {
			return "WrongCommand";
		}
	}

	/**
	* Modify the given string to obtain a result with letters and numbers
	*/
	public String translateNumberInLetter(String resultDeepSpeechSplittedSquare) {
		if(resultDeepSpeechSplittedSquare.equals("1")) {
			return "a";
		} else if(resultDeepSpeechSplittedSquare.equals("2")) {
			return "b";
		} else if(resultDeepSpeechSplittedSquare.equals("3")) {
			return "c";
		} else if(resultDeepSpeechSplittedSquare.equals("4")) {
			return "d";
		} else if(resultDeepSpeechSplittedSquare.equals("5")) {
			return "e";
		} else if(resultDeepSpeechSplittedSquare.equals("6")) {
			return "f";
		} else if(resultDeepSpeechSplittedSquare.equals("7")) {
			return "g";
		} else if(resultDeepSpeechSplittedSquare.equals("8")) {
			return "h";
		} else {
			return "WrongCommand";
		}
	}

	/**
	* Check if the given string is a number between 1 and 8 in a party
	*/
	public String checkNumberForParty(String resultDeepSpeechSplittedSquare) {
		if(resultDeepSpeechSplittedSquare.equals("one") || resultDeepSpeechSplittedSquare.equals("wone") || resultDeepSpeechSplittedSquare.equals("won")) {
			return "1";
		} else if(resultDeepSpeechSplittedSquare.equals("two") || resultDeepSpeechSplittedSquare.equals("too") || resultDeepSpeechSplittedSquare.equals("to")) {
			return "2";
		} else if(resultDeepSpeechSplittedSquare.equals("three") || resultDeepSpeechSplittedSquare.equals("free") || resultDeepSpeechSplittedSquare.equals("thre") || resultDeepSpeechSplittedSquare.equals("fret") || resultDeepSpeechSplittedSquare.equals("thret") || resultDeepSpeechSplittedSquare.equals("frit") || resultDeepSpeechSplittedSquare.equals("fre") || resultDeepSpeechSplittedSquare.equals("flee")) {
			return "3";
		} else if(resultDeepSpeechSplittedSquare.equals("four")  || resultDeepSpeechSplittedSquare.equals("for") || resultDeepSpeechSplittedSquare.equals("forth") || resultDeepSpeechSplittedSquare.equals("fur")) {
			return "4";
		} else if(resultDeepSpeechSplittedSquare.equals("five")  || resultDeepSpeechSplittedSquare.equals("fie") || resultDeepSpeechSplittedSquare.equals("fine")) {
			return "5";
		} else if(resultDeepSpeechSplittedSquare.equals("six")) {
			return "6";
		} else if(resultDeepSpeechSplittedSquare.equals("seven")) {
			return "7";
		} else if(resultDeepSpeechSplittedSquare.equals("eight") || resultDeepSpeechSplittedSquare.equals("ath") || resultDeepSpeechSplittedSquare.equals("hate")) {
			return "8";
		} else {
			return "WrongCommand";
		}
	}

	/**
	* Check if the given string is a number between 1 and 4 in a menu
	*/
	public String checkNumberForMenu(String resultDeepSpeechSplittedSquare) {
		if(resultDeepSpeechSplittedSquare.equals("one") || resultDeepSpeechSplittedSquare.equals("wone") || resultDeepSpeechSplittedSquare.equals("won")) {
			return "1";
		} else if(resultDeepSpeechSplittedSquare.equals("two") || resultDeepSpeechSplittedSquare.equals("too") || resultDeepSpeechSplittedSquare.equals("to")) {
			return "2";
		} else if(resultDeepSpeechSplittedSquare.equals("three") || resultDeepSpeechSplittedSquare.equals("free") || resultDeepSpeechSplittedSquare.equals("thre") || resultDeepSpeechSplittedSquare.equals("fret") || resultDeepSpeechSplittedSquare.equals("thret") || resultDeepSpeechSplittedSquare.equals("frit") || resultDeepSpeechSplittedSquare.equals("fre") || resultDeepSpeechSplittedSquare.equals("flee")) {
			return "3";
		} else if(resultDeepSpeechSplittedSquare.equals("four") || resultDeepSpeechSplittedSquare.equals("for") || resultDeepSpeechSplittedSquare.equals("forth")) {
			return "4";
		} else if(resultDeepSpeechSplittedSquare.equals("five")  || resultDeepSpeechSplittedSquare.equals("fie") || resultDeepSpeechSplittedSquare.equals("fine")) {
			return "5";
		} else if(resultDeepSpeechSplittedSquare.equals("six")) {
			return "6";
		} else if(resultDeepSpeechSplittedSquare.equals("seven")) {
			return "7";
		} else {
			return "WrongCommand";
		}
	}

	/**
	* Check if the given string corresponds to a castling move
	*/
	public String checkPromoteWord(String resultDeepSpeechSplittedSquare) {
		if(resultDeepSpeechSplittedSquare.equals("queen")) {
			return "q";
		} else if(resultDeepSpeechSplittedSquare.equals("rook")) {
			return "r";
		} else if(resultDeepSpeechSplittedSquare.equals("bishop")) {
			return "b";
		} else if(resultDeepSpeechSplittedSquare.equals("night") || resultDeepSpeechSplittedSquare.equals("knight")) {
			return "n";
		} else {
			return "WrongCommand";
		}
	}

	/**
	* Call the other methods and save the result in a file for the game
	*/
	public void SpeechForGameInFile(DisplayBoard d) {
		String finalResult = "";
		SpeechRecorder speechRecorder = new SpeechRecorder();
		FileWriter writer;
		d.displayRecording(true);
		speechRecorder.launchSpeechRecorder(4000);
		d.displayRecording(false);
		d.displayLoading(true);
		callDeepSpeech(audioFileName,d);
		finalResult = obtainResultFromDeepSpeech();
		finalResult = analyzeResultFromDeepSpeechInParty(finalResult);
		if(finalResult.equals("menu"))
		{
			String finalResultFileName = "quit.txt";
			File finalResultFile = new File(finalResultFileName);
			try {
				writer = new FileWriter(finalResultFile);
				writer.write(finalResult);
				writer.close();
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
		else
		{
			String finalResultFileName = "move.txt";
			File finalResultFile = new File(finalResultFileName);
			try {
				writer = new FileWriter(finalResultFile);
				writer.write(finalResult);
				writer.close();
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	* Call the other methods and save the result in a file for the menu
	*/
	public void SpeechForMenuInFile(DisplayMenu dm) {
		String finalResult = "";
		String finalResultFileName = "menu.txt";
		File finalResultFile = new File(finalResultFileName);
		FileWriter writer;
		dm.displayRecording(true);
		SpeechRecorder speechRecorder = new SpeechRecorder();
		speechRecorder.launchSpeechRecorder(2000);
		dm.displayLoading(true);
		dm.displayRecording(false);
		callDeepSpeech(audioFileName,null);
		finalResult = obtainResultFromDeepSpeech();
		finalResult = analyzeResultFromDeepSpeechInMenu(finalResult);
		try {
			writer = new FileWriter(finalResultFile);
			writer.write(finalResult);
			writer.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

	/**
	* Method to launch listening
	*/
	public void SpeechRecognizerLaunch(String t,DisplayBoard d,DisplayMenu dm) {
		if(t.equals("Menu")) {
			System.out.println("Menu");
			SpeechForMenuInFile(dm);
			dm.displayLoading(false);
		} else if(t.equals("Game")) {
			SpeechForGameInFile(d);
			d.displayLoading(false);
		}
	}
}

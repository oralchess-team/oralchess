
import java.io.File;
import java.io.IOException;

import java.lang.InterruptedException;

import javax.sound.sampled.TargetDataLine;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.LineUnavailableException;

public class SpeechRecorder {
	
	private TargetDataLine tdl;

	/**
	* Define an audio format and return it
	*/
	public AudioFormat getAudioFormat() {
		AudioFormat audioFormat = new AudioFormat(16000, 16, 1, true, true);
		return audioFormat;
	}

	/**
	* Capture the sound and record it into a WAVE file
	*/
	public void startRecord() {
		String audioFileName = "audioRecordFile.wav";
		File audioFile = new File(audioFileName);
		AudioFileFormat.Type audioFileType = AudioFileFormat.Type.WAVE;
		AudioFormat audioFormat = getAudioFormat();
		DataLine.Info info = new DataLine.Info(TargetDataLine.class, audioFormat);
		try {
			tdl = (TargetDataLine) AudioSystem.getLine(info);
			tdl.open(audioFormat);
			tdl.start();
			AudioInputStream ais = new AudioInputStream(tdl);
			AudioSystem.write(ais, audioFileType, audioFile);
		} catch(LineUnavailableException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

	/**
	* Stop the recording
	*/
	public void finishRecord() {
		tdl.stop();
		tdl.close();
	}

	/**
	* Launch the speech recorder
	*/
	public void launchSpeechRecorder(int time) {
		SpeechRecorder recorder = new SpeechRecorder();
		int recordTime = time;
		Thread waitFor = new Thread(new Runnable() {
			public void run() {
				try {
					Thread.sleep(recordTime);
				} catch(InterruptedException e) {
					e.printStackTrace();
				}
				recorder.finishRecord();
			}
		});
		waitFor.start();
		recorder.startRecord();
	}
}